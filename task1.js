const tasks = [

  {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},

  {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},

  {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},

  {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},

  {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},

  {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},

  {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},

];

const front = tasks.reduce((accum, currentValue) => {
  if(currentValue.category === 'Frontend') {
    accum += currentValue.timeSpent;
  }
  return accum;
}, 0);

const bug = tasks.reduce((accum, currentValue) => {
  if(currentValue.type === 'bug') {
    accum += currentValue.timeSpent;
  }
  return accum;
}, 0);

const taskWithUi = tasks.filter(task => task.title.includes('UI')).length;

const allTask = tasks.reduce((accum, currentVal) => {
  if(currentVal.category === 'Frontend') {
    accum.Fronted++;
  }
  if(currentVal.category === 'Backend') {
    accum.Backend++;
  }
  return accum;
}, {Fronted: 0, Backend: 0});

const logtTimeTasks = tasks.map((task) => {
  if(task.timeSpent >= 4){
    return {title: task.title, category: task.category}
  }
}).filter((item) => {
  if(item !== undefined) {
    return item;
  }
});

console.log(front);
console.log(bug);
console.log(taskWithUi);
console.log(allTask);
console.log(logtTimeTasks);

