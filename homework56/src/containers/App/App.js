import React, { Component } from 'react';

import './App.css';
import GameBuild from "../GameBild/GameBuild";

class App extends Component {

  render() {
    return (
      <div className="App">
        <GameBuild/>
      </div>
    );
  }
}

export default App;
