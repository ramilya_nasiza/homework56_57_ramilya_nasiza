import React, {Component, Fragment} from 'react';
import Cage from "../../components/Cage/Cage";
import './GameBuild.css';
import TriesCounter from "../../components/TriesCounter/TriesCounter";

class GameBuild extends Component {

  renderFields = () => {
    const array = [];
    for(let i = 0; i < 36; i ++) {
      let object = {isClicked: false, isRing: false, show:false};
      array.push(object);
    }
    let id = Math.floor(Math.random() * array.length);
    array[id].isRing = true;
    return array;
  };

  state ={
    cages: this.renderFields(),
    tries: 0
  };

  clickHandler = (id) => {
    let cages = this.state.cages;
    cages[id].isClicked = true;

    let tries = this.state.tries;
    tries++;
    if (cages[id].isRing) cages[id].show = true;

    this.setState({cages});
    this.setState({tries: tries});
  };

  render() {
    return (
        <div>
          <div className='field'>
            {this.state.cages.map((item, id) => <Cage key={id} click={() => this.clickHandler(id) } hide={item.isClicked? "hide" : ""} ring={item.show? "ring" : ""} />)}
          </div>
          <TriesCounter tries={this.state.tries}/>
        </div>

    )
  }
}

export default GameBuild;
