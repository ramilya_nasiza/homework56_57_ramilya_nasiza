import React from 'react';

import './Cage.css';

const Cage = props => {
  return (
      <div onClick={props.click} className={`cage ${props.hide} ${props.ring}`}>
      </div>
  );
};

export default Cage;