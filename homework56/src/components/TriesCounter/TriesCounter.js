import React from 'react';

const TriesCounter = props => {
  return(
      <p>Your Tries: {props.tries}</p>
  );
};

export default TriesCounter;