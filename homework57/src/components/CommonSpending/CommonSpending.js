import React from 'react';

import './Commonspending.css';

const CommonSpending = props => {
  return (
      props.allspends.map((oneSpend, index) => {
        return (
            <div key={index} className='commonSpends'>
              <span className='spanItemName'>{oneSpend.item}</span>
              <span>{oneSpend.cost} KGS</span>
              <button onClick={() => props.delete(oneSpend.id)}>X</button>
            </div>
        )
      })
  )
};

export default CommonSpending;