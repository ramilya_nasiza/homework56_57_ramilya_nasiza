import React from 'react';

import './TotalSpend.css';

const TotalSpend = props => {
  return(
      <div className='total'>
        Total spend:
        <p>{props.total}</p>
      </div>
  );
};

export default TotalSpend;