import React from 'react';

import './NewSpending.css';

const NewSpending = props => {
  return(
      <div className='new-spend'>
        <input type="text" className='item-name' onChange={props.change}/>
        <span><input type='number' className='cost'onChange={props.change2} /> KGS</span>
        <button className='addBtn' onClick={props.add}>Add</button>
      </div>
  );
};

export default NewSpending;