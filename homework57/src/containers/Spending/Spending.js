import React, { Component } from 'react';

import NewSpending from "../../components/NewSpending/NewSpending";
import CommonSpending from "../../components/CommonSpending/CommonSpending";
import TotalSpend from "../../components/TotalSpend/TotalSpend";

class Spending extends Component {

  state = {
    spends: [],
    item: '',
    cost: ''
  };

  changeHandler = (event) => {
    this.setState({
      item: event.target.value,
    })
  };

  changeHandler2 = (event) => {
    this.setState({
      cost: event.target.value
    })
  };

  addNewSpend = () =>{
    const spends = [...this.state.spends];
    if(this.state.item !== '' && this.state.cost !== '') {
      const oneSpend = {item: this.state.item, cost: this.state.cost, id: spends.length + 1};
      spends.push(oneSpend);

      this.setState({spends: spends});
    } else {
      alert('Fill all fields')
    }
  };

  removeOneSpend = (id) => {
    const spends = [...this.state.spends];
    const spendId = spends.findIndex(oneSpend => oneSpend.id === id);

    spends.splice(spendId, 1);

    this.setState({spends});
  };

  getTotal = () => {
    const arrTotal = [];

    this.state.spends.map((oneCost) => {
      return arrTotal.push(parseInt(oneCost.cost));
    });

    const sum = arrTotal.reduce(function (accumulator, currentValue) {
      return accumulator + currentValue;
    }, 0);

    return sum;

  };

  render() {
    return (
        <div className="App">
          <NewSpending change={(event) => this.changeHandler(event)} change2={(event) => this.changeHandler2(event)} add={() => this.addNewSpend()}/>
          <div className='list'>
            <CommonSpending allspends={this.state.spends} delete={this.removeOneSpend}/>
            <TotalSpend total={this.getTotal()}/>
          </div>
        </div>
    );
  }
}

export default Spending;
